# Button

```js
import { Button } from '@martijn/buttons';
```

## Examples

<!-- STORY -->

```js
<Button>My Primary Button</Button>
<Button secondary>My Secondary Button</Button>
```
