import Button from './components/Button.vue';

export const components = {
  Button
};

export default {
  install(Vue) {
    Object.keys(components).forEach(name => {
      Vue.component(name, components[name]);
    });
  }
};

export { Button };
