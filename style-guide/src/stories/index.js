import ButtonREADME from '@martijn/buttons/docs/README.md';
import { storiesOf } from '@storybook/vue';
import { withDocs } from 'storybook-readme';

storiesOf('Buttons', module)
  .addDecorator(withDocs(ButtonREADME))
  .add('Button', () => ({
    template: `
      <div>
        <Button>My Primary Button</Button>
        <Button secondary>My Secondary Button</Button>
      </div>
    `
  }));
