module.exports = (baseConfig, env, defaultConfig) => {
  // setup scss support
  defaultConfig.module.rules.push({
    test: /\.scss$/,
    use: ['vue-style-loader', 'css-loader', 'sass-loader']
  });

  return defaultConfig;
};
