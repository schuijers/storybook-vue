import { Button } from '@martijn/buttons';
import { configure } from '@storybook/vue';
import Vue from 'vue';

// import global styles
import '@martijn/core/src/styles/index.scss';

// register components
Vue.component('Button', Button);

function loadStories() {
  require('../src/stories');
}

configure(loadStories, module);
